#include <SoftwareSerial.h>

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  Serial.print("A0 = ");
  Serial.print(analogRead(A0));
  delay(10);
  Serial.print("     A1 = ");
  Serial.print(analogRead(A1));
  delay(10);
  Serial.print("     A2 = ");
  Serial.print(analogRead(A2));
  delay(10);
  Serial.print("     A3 = ");
  Serial.print(analogRead(A3));
  delay(10);
  Serial.print("     A4 = ");
  Serial.println(analogRead(A4));
  delay(10);
  Serial.print("     A5 = ");
  Serial.println(analogRead(A5));
  delay(1000);
}
