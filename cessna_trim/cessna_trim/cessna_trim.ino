#include <Servo.h>
#include <SoftwareSerial.h>
const int encoderPin1 = 2;
const int encoderPin2 = 3;
const int trimUp = 4;
const int trimDown = 5;
const int motorDirPin = 7;
const int motorStepPin = 6;
// servo control pin 9 and 10 only
const int servoPin = 9;

String inString = "";    // string to hold input
float trimAngle = 0.00;
float servoAngle = 0.00;
Servo trimServo;
int pin1Val, pin2Val;
char incomingByte[10];
int terminateByte = 10;
volatile int lastEncoded = 0;
volatile long encoderValue = 0;
long lastencoderValue = 0;
int lastMSB = 0;
int lastLSB = 0;
String encoderDirection = "";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(motorStepPin, OUTPUT);
  pinMode(motorDirPin, OUTPUT);
  pinMode(trimDown, INPUT_PULLUP);
  pinMode(trimUp, INPUT_PULLUP);
  pinMode(encoderPin1, INPUT);
  pinMode(encoderPin2, INPUT);
  digitalWrite(encoderPin1, HIGH); //turn pullup resistor on
  digitalWrite(encoderPin2, HIGH); //turn pullup resistor on
  attachInterrupt(0, updateEncoder, CHANGE); 
  attachInterrupt(1, updateEncoder, CHANGE);
  trimServo.attach(servoPin);
}

void loop() {
  // STEP 1: move motor from yoke trim
  if (digitalRead(trimDown) == LOW) {
    digitalWrite(motorDirPin, LOW);
    digitalWrite(motorStepPin, HIGH);
  Serial.println("motor down");
  }
  if (digitalRead(trimUp) == LOW) {
    digitalWrite(motorDirPin, HIGH);
    digitalWrite(motorStepPin, HIGH);
  Serial.println("motor up");
  }
  delayMicroseconds(500);
  digitalWrite(motorStepPin, LOW);
  delayMicroseconds(500);

  // STEP 2: detect rotary movement
  // leave blank, using interrupt to detect

  // STEP 3: read current trim position from host
  while (Serial.available() > 0) {
    int inChar = Serial.read();
    // read string until newline found
    if (inChar != '\n') {
      inString += (char)inChar;
    } else {
      trimAngle = inString.toFloat();
      // clear the string for new input:
      inString = "";
    }
  }

  // STEP 4: move servo trim position
  servoAngle = (15 + trimAngle) * (180 / 30);
  trimServo.write(servoAngle);
}

void updateEncoder(){
  int MSB = digitalRead(encoderPin1); //MSB = most significant bit
  int LSB = digitalRead(encoderPin2); //LSB = least significant bit

  int encoded = (MSB << 1) |LSB; //converting the 2 pin value to single number
  int sum  = (lastEncoded << 2) | encoded; //adding it to the previous encoded value

  if(sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) {
    encoderValue ++;
    encoderDirection = "2";
  }
  if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000){
    encoderValue --;
    encoderDirection = "1";
  }

  lastEncoded = encoded; //store this value for next time
  Serial.println(encoderDirection);
}
