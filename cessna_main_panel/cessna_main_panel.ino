#include <SoftwareSerial.h>
#include <Servo.h>

const int throttleAxis = A0;
const int mixtureAxis = A1;
const int flapsAxis = A2;
const int flapsServoPin = 9;
const int starterOff = 2;
const int starterRight = 3;
const int starterLeft = 4;
const int starterBoth = 5;
const int starterStart = 6;

String hardwareInput = "";
String inString = "";    // string to hold input
Servo flapsServo;

void setup() {
  // put your setup code here, to run once:

  pinMode(starterOff, INPUT_PULLUP);
  pinMode(starterRight, INPUT_PULLUP);
  pinMode(starterLeft, INPUT_PULLUP);
  pinMode(starterBoth, INPUT_PULLUP);
  pinMode(starterStart, INPUT_PULLUP);
  flapsServo.attach(flapsServoPin);
  Serial.begin(9600);
}

void loop() {
  hardwareInput = "";
  // put your main code here, to run repeatedly:
  if (digitalRead(starterOff) == LOW) {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(starterRight) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  }
  if (digitalRead(starterLeft) == LOW) {
    hardwareInput = String(hardwareInput + "2");
  }
  if (digitalRead(starterBoth) == LOW) {
    hardwareInput = String(hardwareInput + "3");
  }
  if (digitalRead(starterStart) == LOW) {
    hardwareInput = String(hardwareInput + "4");
  }

  hardwareInput = String(hardwareInput + "/");
  hardwareInput = String(hardwareInput + analogRead(throttleAxis)); 
  delay(10);
  hardwareInput = String(hardwareInput + "/");
  hardwareInput = String(hardwareInput + analogRead(mixtureAxis));
  delay(10);
  hardwareInput = String(hardwareInput + "/");
  hardwareInput = String(hardwareInput + analogRead(flapsAxis));
  delay(10);
  hardwareInput = String(hardwareInput + "/");
  hardwareInput = String(hardwareInput + flapsServo.read());

  Serial.println(hardwareInput);
  delay(100);

  // update flaps position indicator
  while (Serial.available() > 0) {
    int inChar = Serial.read();
    // read string until newline found
    if (inChar != '\n') {
      inString += (char)inChar;
    } else {
      flapsServo.write(inString.toInt());
      // clear the string for new input:
      inString = "";
    }
  }
}
