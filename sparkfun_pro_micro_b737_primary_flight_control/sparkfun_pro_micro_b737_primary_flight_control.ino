// Simple example application that shows how to read four Arduino
// digital pins and map them to the USB Joystick library.
//
// The digital pins 9, 10, 11, and 12 are grounded when they are pressed.
//
// NOTE: This sketch file is for use with Arduino Leonardo and
//       Arduino Micro only.
//
// by Matthew Heironimus
// 2015-11-20
//--------------------------------------------------------------------

#include <Joystick.h>

// Create Joystick
Joystick_ Joystick;
const int potensioXAxis = A3;
const int potensioYAxis = A2;
const int potensioZAxis = A1;
const int potensioRxAxis = A0;
const int potensioRyAxis = A10;
const int potensioRzAxis = A6;

void setup() {
  // Initialize Button Pins
  pinMode(2, INPUT_PULLUP); // trim
  pinMode(3, INPUT_PULLUP); // trim
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);
  pinMode(7, INPUT_PULLUP); // trim
  pinMode(8, INPUT_PULLUP); // trim
  pinMode(9, INPUT_PULLUP);
  pinMode(16, INPUT_PULLUP);


  Joystick.setXAxisRange(0, 1023);
  Joystick.setYAxisRange(0, 1023);
  Joystick.setZAxisRange(0, 1023);
  Joystick.setRxAxisRange(0, 1023);
  Joystick.setRyAxisRange(0, 1023);
  Joystick.setRzAxisRange(0, 1023);
  // Initialize Joystick Library
  Joystick.begin();
}

void loop() {
    // Read pin values
  if ((digitalRead(2) == LOW) && (digitalRead(3) == LOW)) {
    Joystick.setButton(0, LOW);
    Joystick.setButton(1, HIGH);
  } else if ((digitalRead(2) == LOW) && (digitalRead(3) == HIGH)) {
    Joystick.setButton(0, HIGH);
    Joystick.setButton(1, LOW);
  } else {
    Joystick.setButton(0, LOW);
    Joystick.setButton(1, LOW);
  }
  Joystick.setButton(2, !digitalRead(5));
  Joystick.setButton(3, !digitalRead(6));
  if ((digitalRead(7) == LOW) && (digitalRead(8) == LOW)) {
    Joystick.setButton(4, LOW);
    Joystick.setButton(5, HIGH);
  } else if ((digitalRead(7) == LOW) && (digitalRead(8) == HIGH)) {
    Joystick.setButton(4, HIGH);
    Joystick.setButton(5, LOW);
  } else {
    Joystick.setButton(4, LOW);
    Joystick.setButton(5, LOW);
  }
  Joystick.setButton(6, !digitalRead(9));
  Joystick.setButton(7, !digitalRead(16));
  
    long X_axis = analogRead(potensioXAxis);
    Joystick.setXAxis(X_axis);
    
    long Y_axis = analogRead(potensioYAxis);
    Joystick.setYAxis(Y_axis);

    long Z_axis = analogRead(potensioZAxis);
    Joystick.setZAxis(Z_axis);
    
    long Rx_axis = analogRead(potensioRxAxis);
    Joystick.setRxAxis(Rx_axis);

    long Ry_axis = analogRead(potensioRyAxis);
    Joystick.setRyAxis(Ry_axis);
    
    long Rz_axis = analogRead(potensioRzAxis);
    Joystick.setRzAxis(Rz_axis);

}
