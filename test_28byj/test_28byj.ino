const int IN1 = 8;
const int IN2 = 9;
const int IN3 = 10;
const int IN4 = 11;
int current_step = 1;
int delay_step_speed = 2000;

void setup() {
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);

  switch (current_step) {
    case 1:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN4, HIGH);
      break;
    case 2:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      break;
    case 3:
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      break;
    case 4:
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      break;
  }
}

void loop() {
  rotateCW();
}

void rotateCCW() {
  switch (current_step) {
    case 1:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      current_step = 2;
      break;
    case 2:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      current_step = 3;
      break;
    case 3:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      current_step = 4;
      break;
    case 4:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      current_step = 1;
      break;
  }
  delay(delay_step_speed);
}

void rotateCW() {
  switch (current_step) {
    case 1:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      current_step = 4;
      break;
    case 2:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      current_step = 1;
      break;
    case 3:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      current_step = 2;
      break;
    case 4:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      current_step = 3;
      break;
  }
  delayMicroseconds(delay_step_speed);
}

