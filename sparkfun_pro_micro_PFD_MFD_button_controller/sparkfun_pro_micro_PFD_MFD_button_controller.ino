#include <Joystick.h>

Joystick_ Joystick;

const int SN74HC165_RESET = 2;
const int SN74HC165_CLOCK = 3;
const int SN74HC165_DATA = 4;
const int numberOf74HC165 = 4;

void setup() {
  // put your setup code here, to run once:
  pinMode(SN74HC165_RESET, OUTPUT);
  pinMode(SN74HC165_CLOCK, OUTPUT);
  pinMode(SN74HC165_DATA, INPUT);

  Joystick.begin();
}

// Last state of the button
int lastButtonState[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void loop() {
  // put your main code here, to run repeatedly:
  reset_74HC165();
  for (int i = 0; i < 8 * numberOf74HC165; i++) {
    int currentButtonState = digitalRead(SN74HC165_DATA);
    if (currentButtonState != lastButtonState[index])
    {
      Joystick.setButton(i, currentButtonState);
      lastButtonState[i] = currentButtonState;
    }
    clock_74HC165();
  }
}

void reset_74HC165() {
  digitalWrite(SN74HC165_RESET, LOW);
  digitalWrite(SN74HC165_RESET, HIGH);
}

void clock_74HC165() {
  digitalWrite(SN74HC165_CLOCK, HIGH);
  digitalWrite(SN74HC165_CLOCK, LOW);
}

