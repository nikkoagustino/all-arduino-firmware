
#include "Keyboard.h"

const int ptt_capt = 2;
const int ptt_fo = 3;

void setup() {
  // put your setup code here, to run once:
  pinMode(ptt_capt, INPUT_PULLUP);
  pinMode(ptt_fo, INPUT_PULLUP);
  Keyboard.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
    if ((digitalRead(ptt_capt) == LOW) || (digitalRead(ptt_fo) == LOW)) {
      Keyboard.press(KEY_F12);
      delay(100);
    } else {
      Keyboard.release(KEY_F12);
    }
}
