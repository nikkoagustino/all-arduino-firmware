#include <EEPROM.h>

String received_serial;
float received_heading = 0;
const int shutdown_signal_pin = 2;
String inString = "";
const int IN1 = 12;
const int IN2 = 11;
const int IN3 = 10;
const int IN4 = 9;
const int FULL_ROTATION_STEP = 2048;
const int HALF_ROTATION_STEP = 1024;
int active_step_byte = 1;
int current_step = 0;
float step_to_go = 0;
int delay_step_speed = 2000;
int current_heading = 0;
int step_difference = 0;

void setup() {  
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);

  Serial.begin(9600);
//  readEEPROM();
//  attachInterrupt(0, saveEEPROM, FALLING);
  
  switch (active_step_byte) {
    case 1:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN4, HIGH);
      break;
    case 2:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      break;
    case 3:
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      break;
    case 4:
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      break;
  }
}

void loop() {
  readSerial();
  received_heading = received_serial.toFloat();
  if (received_heading > 360) {
    calibrationMode();
  } else {
    rotateCompass();
  }
}

void calibrationMode() {
  switch ((int) received_heading) {
    case 990: // calibration mode start
      break;
    case 991: // interface shutdown
      saveEEPROM();
      break;
    case 997:
      rotateCCW();
      break;
    case 998:
      rotateCW();
      break;
    case 999:
      current_heading = 0;
      current_step = 0;
      break;
  }
}

void rotateCompass() {
  step_to_go = received_heading / 360.0 * FULL_ROTATION_STEP;
  step_difference = (int) step_to_go - current_step;
  if (step_difference == 0) {
    // do nothing
  } else if (((step_difference > 0) && (step_difference < HALF_ROTATION_STEP)) || ((step_difference > FULL_ROTATION_STEP * -1) && (step_difference < HALF_ROTATION_STEP * -1))) {
    rotateCCW();
  } else {
    rotateCW();
  }

  if (current_step == FULL_ROTATION_STEP) {
    current_step = 0;
  }
  if (current_step == -1) {
    current_step = FULL_ROTATION_STEP - 1;
  }
}

void rotateCCW() {
  switch (active_step_byte) {
    case 1:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      active_step_byte = 2;
      break;
    case 2:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      active_step_byte = 3;
      break;
    case 3:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      active_step_byte = 4;
      break;
    case 4:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      active_step_byte = 1;
      break;
  }
  current_step++;
  delayMicroseconds(delay_step_speed);
}

void rotateCW() {
  switch (active_step_byte) {
    case 1:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      active_step_byte = 4;
      break;
    case 2:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      active_step_byte = 1;
      break;
    case 3:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      active_step_byte = 2;
      break;
    case 4:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      active_step_byte = 3;
      break;
  }
  current_step--;
  delayMicroseconds(delay_step_speed);
}

void readSerial() {
  while (Serial.available() > 0) {
    int inChar = Serial.read();
    // read string until newline found
    if (inChar != '\n') {
      inString += (char)inChar;
    } else {
      received_serial = inString;
      // clear the string for new input:
      inString = "";
    }
  }
}

void readEEPROM() {
  current_step = (EEPROM.read(0) * 100) + EEPROM.read(1);
  active_step_byte = EEPROM.read(2);
}

void saveEEPROM() {
  EEPROM.update(0, floor(current_step / 100));
  EEPROM.update(1, current_step % 100);
  EEPROM.update(2, active_step_byte);
}

