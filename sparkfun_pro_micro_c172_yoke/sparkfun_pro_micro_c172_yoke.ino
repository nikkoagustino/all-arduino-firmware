// Simple example application that shows how to read four Arduino
// digital pins and map them to the USB Joystick library.
//
// The digital pins 9, 10, 11, and 12 are grounded when they are pressed.
//
// NOTE: This sketch file is for use with Arduino Leonardo and
//       Arduino Micro only.
//
// by Matthew Heironimus
// 2015-11-20
//--------------------------------------------------------------------

#include <Joystick.h>
#include "Keyboard.h"

// Create Joystick
Joystick_ Joystick;
const int potensioXAxis = A0;
const int potensioYAxis = A1;

void setup() {
  // Initialize Button Pins
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);


  Joystick.setXAxisRange(0, 1023);
  Joystick.setYAxisRange(0, 1023);
  // Initialize Joystick Library
  Joystick.begin();
  Keyboard.begin();
}

void loop() {

  if ((digitalRead(2) == LOW) || (digitalRead(3) == LOW)) {
    Keyboard.press('p');
  } else {
    Keyboard.release('p');
  }
  
    long X_axis = analogRead(potensioXAxis);
    Joystick.setXAxis(X_axis);
    
    long Y_axis = analogRead(potensioYAxis);
    Joystick.setYAxis(Y_axis);

}
