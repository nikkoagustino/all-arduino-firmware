#include <Servo.h>
  Servo trimServo;
const int servoPin = 9;
void setup() {
  // put your setup code here, to run once:
}

void loop() {
  // put your main code here, to run repeatedly:
  trimServo.attach(servoPin);
  trimServo.write(180);
  delay(5000);
  trimServo.write(90);
  trimServo.detach();
  delay(3000);
}
