#include <SoftwareSerial.h>

const int togParkingBrake = 2;
const int togSpoilerArm = 3;
const int togLandingGear = 4;

const int axisSpoiler = A0;
const int axisThrottleA = A1;
const int axisThrottleB = A2;

int lastStateParkingBrake = 0;
int lastStateSpoilerArm = 0;
int lastStateLandingGear = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(togParkingBrake, INPUT_PULLUP);
  pinMode(togSpoilerArm, INPUT_PULLUP);
  pinMode(togLandingGear, INPUT_PULLUP);

  lastStateParkingBrake = digitalRead(togParkingBrake);
  lastStateSpoilerArm = digitalRead(togSpoilerArm);
  lastStateLandingGear = digitalRead(togLandingGear);
  
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if ((lastStateParkingBrake == HIGH) && (digitalRead(togParkingBrake) == LOW)) {
    lastStateParkingBrake = LOW;
    Serial.println("Parking Brake ON");
  } else if ((lastStateParkingBrake == LOW) && (digitalRead(togParkingBrake) == HIGH)) {
    lastStateParkingBrake = HIGH;
    Serial.println("Parking Brake OFF");
  }

  if ((lastStateSpoilerArm == HIGH) && (digitalRead(togSpoilerArm) == LOW)) {
    lastStateSpoilerArm = LOW;
    Serial.println("Spoiler ARM");
  } else if ((lastStateSpoilerArm == LOW) && (digitalRead(togSpoilerArm) == HIGH)) {
    lastStateSpoilerArm = HIGH;
    Serial.println("Spoiler DISARM");
  }

  if ((lastStateLandingGear == HIGH) && (digitalRead(togLandingGear) == LOW)) {
    lastStateLandingGear = LOW;
    Serial.println("Gear DOWN");
  } else if ((lastStateLandingGear == LOW) && (digitalRead(togLandingGear) == HIGH)) {
    lastStateLandingGear = HIGH;
    Serial.println("Gear UP");
  }

  // arithmetic operation to send axis to flight simulator
  float axs = analogRead(axisSpoiler);
  float axa = analogRead(axisThrottleA);
  float axb = analogRead(axisThrottleB);
  int FS_axisSpoiler = (float) (axs * 16383 / 1023);
  int FS_axisThrottleA = (float) ((axa * 20480 / 1023) - 4096);
  int FS_axisThrottleB = (float) ((axb * 20480 / 1023) - 4096);

  if ((FS_axisThrottleA < 200) && (FS_axisThrottleA > -200)) {
    FS_axisThrottleA = 0;
  }

  if ((FS_axisThrottleB < 200) && (FS_axisThrottleB > -200)) {
    FS_axisThrottleB = 0;
  }

  Serial.println("SpoilerAxis_" + String(FS_axisSpoiler));
  Serial.println("ThrottleAAxis_" + String(FS_axisThrottleA));
  Serial.println("ThrottleBAxis_" + String(FS_axisThrottleB));
  delay(100);
}
