// Simple example application that shows how to read four Arduino
// digital pins and map them to the USB Joystick library.
//
// The digital pins 9, 10, 11, and 12 are grounded when they are pressed.
//
// NOTE: This sketch file is for use with Arduino Leonardo and
//       Arduino Micro only.
//
// by Matthew Heironimus
// 2015-11-20
//--------------------------------------------------------------------

#include <Joystick.h>
#include <SoftwareSerial.h>

// Create Joystick
Joystick_ Joystick;
const int potensioZAxis = A0;
const int potensioXAxis = A1;
const int potensioYAxis = A2;
//const int potensioZRotation = A3;
//const int potensioThrottle = A5;

void setup() {
  // Initialize Button Pins
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  pinMode(6, INPUT_PULLUP);

  Joystick.setXAxisRange(0, 800);
  Joystick.setYAxisRange(0, 800);
  Joystick.setZAxisRange(0, 1024);
  
  Joystick.setRxAxisRange(900, 1024);
  Joystick.setRyAxisRange(900, 1024);
  // Initialize Joystick Library
  Joystick.begin();
  Serial.begin(9600);
}

// Constant that maps the phyical pin to the joystick button.
const int pinToButtonMap = 2;

// Last state of the button
int lastButtonState[5] = {0,0,0,0,0};

void loop() {

  // Read pin values
  for (int index = 0; index < 5; index++)
  {
    int currentButtonState = !digitalRead(index + pinToButtonMap);
    if (currentButtonState != lastButtonState[index])
    {
      Joystick.setButton(index, currentButtonState);
      lastButtonState[index] = currentButtonState;
    }
  }

  // X Y Z axis use -127 to 127
  if (analogRead(potensioXAxis) < 800) {
    long X_axis = analogRead(potensioXAxis);
    Joystick.setXAxis(X_axis);
  } else {  
    long RX_axis = analogRead(potensioXAxis);
    Joystick.setRxAxis(RX_axis);
  }

  
  if (analogRead(potensioYAxis) < 800) {
    long Y_axis = analogRead(potensioYAxis);
    Joystick.setYAxis(Y_axis);
  } else {
    long RY_axis = analogRead(potensioYAxis);
    Joystick.setRyAxis(RY_axis);
  }
  
  long Z_axis = analogRead(potensioZAxis);
  Joystick.setZAxis(Z_axis);


  // X Y Z rotation use 0 to 360
//  long Z_rotation = (analogRead(potensioZRotation) * 360L) / 1024L;
//  Joystick.setZAxisRotation(Z_rotation);

  // throttle rudder slider use 0 to 255
//  long throttle_axis = (analogRead(potensioThrottle) * 255L) / 1024L;
//  Joystick.setThrottle(throttle_axis);
}
