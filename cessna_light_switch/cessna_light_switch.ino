#include <SoftwareSerial.h>

// 7 toggle switch
const int beaconLightSwitch = 2;
const int landingLightSwitch = 3;
const int taxiLightSwitch = 4;
const int navLightSwitch = 5;
const int strobeLightSwitch = 6;
const int fuelPumpSwitch = 7;
const int pitotHeatSwitch = 8;
// 4 rocker switch
const int alternatorSwitch = 9;
const int batterySwitch = 10;
const int avionic1Switch = 11;
const int avionic2Switch = 12;
// 1 standby battery
const int standbyBattArmSwitch = 13;
String hardwareInput = "";

void setup() {
  // put your setup code here, to run once:
  // 7 toggle switch
  pinMode(beaconLightSwitch, INPUT_PULLUP);
  pinMode(landingLightSwitch, INPUT_PULLUP);
  pinMode(taxiLightSwitch, INPUT_PULLUP);
  pinMode(navLightSwitch, INPUT_PULLUP);
  pinMode(strobeLightSwitch, INPUT_PULLUP);
  pinMode(fuelPumpSwitch, INPUT_PULLUP);
  pinMode(pitotHeatSwitch, INPUT_PULLUP);
  // 4 rocker switch
  pinMode(alternatorSwitch, INPUT_PULLUP);
  pinMode(batterySwitch, INPUT_PULLUP);
  pinMode(avionic1Switch, INPUT_PULLUP);
  pinMode(avionic2Switch, INPUT_PULLUP);
  // 1 standby battery
  pinMode(standbyBattArmSwitch, INPUT_PULLUP);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  hardwareInput = "";
  if (digitalRead(beaconLightSwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(landingLightSwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(taxiLightSwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(navLightSwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(strobeLightSwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(fuelPumpSwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(pitotHeatSwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(alternatorSwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(batterySwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(avionic1Switch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(avionic2Switch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  if (digitalRead(standbyBattArmSwitch) == LOW) {
    hardwareInput = String(hardwareInput + "1");
  } else {
    hardwareInput = String(hardwareInput + "0");
  }
  Serial.println(hardwareInput);
  delay(100);
}
