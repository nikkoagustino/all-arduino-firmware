#include <Servo.h>
#include <SoftwareSerial.h>
const int encoderPin1 = 2;
const int encoderPin2 = 3;
const int trimUp = 4;
const int trimDown = 5;
const int IN1 = 9;
const int IN2 = 8;
const int IN3 = 7;
const int IN4 = 6;
const int motorServoPin = 10;
const int relayActivationPin = 19;
const int positionPotensio = A4;

String inString = "";    // string to hold input
int trimAngle = 0;
Servo motorServo;
int active_step_byte = 1;
int potAngle, pin1Val, pin2Val;
char incomingByte[10];
int terminateByte = 10;
int delay_step_speed = 2000;
volatile int lastEncoded = 0;
volatile long encoderValue = 0;
long lastencoderValue = 0;
int lastMSB = 0;
int lastLSB = 0;
String encoderDirection = "";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(trimDown, INPUT_PULLUP);
  pinMode(trimUp, INPUT_PULLUP);
  pinMode(encoderPin1, INPUT);
  pinMode(encoderPin2, INPUT);
  pinMode(relayActivationPin, OUTPUT);
  pinMode(motorServoPin, OUTPUT);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
  digitalWrite(relayActivationPin, LOW);
  digitalWrite(encoderPin1, HIGH); //turn pullup resistor on
  digitalWrite(encoderPin2, HIGH); //turn pullup resistor on
  attachInterrupt(digitalPinToInterrupt(encoderPin1), updateEncoder, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encoderPin2), updateEncoder, CHANGE);
  motorServo.attach(motorServoPin);
  turnOffServoMotor();
}

void turnOffServoMotor() {
  motorServo.write(90);
  digitalWrite(relayActivationPin, HIGH);
}

void servoMotorCW() {
  motorServo.write(95);
  digitalWrite(relayActivationPin, LOW);
}

void servoMotorCCW() {
  motorServo.write(75);
  digitalWrite(relayActivationPin, LOW);
}

void loop() {
  // STEP 1: move motor from yoke trim
  if (digitalRead(trimDown) == LOW) {
    servoMotorCW();
  }
  if (digitalRead(trimUp) == LOW) {
    servoMotorCCW();
  }
  if (digitalRead(trimUp) == HIGH && digitalRead(trimDown) == HIGH) {
    turnOffServoMotor();
  }

  // STEP 2: detect rotary movement
  // leave blank, using interrupt to detect

  // STEP 3: read current trim position from host
  while (Serial.available() > 0) {
    int inChar = Serial.read();
    // read string until newline found
    if (inChar != '\n') {
      inString += (char)inChar;
    } else {
      trimAngle = inString.toFloat();
      // clear the string for new input:
      inString = "";
    }
  }

  // STEP 4: move trim position
  if ((trimAngle > 0) && (trimAngle < 1024)){
    potAngle = analogRead(positionPotensio);
    if (trimAngle < potAngle - 2) {
      rotateCW();
    } else if (trimAngle > potAngle + 2) {
      rotateCCW();
    }
  }
}

void updateEncoder() {
  int MSB = digitalRead(encoderPin1); //MSB = most significant bit
  int LSB = digitalRead(encoderPin2); //LSB = least significant bit

  int encoded = (MSB << 1) | LSB; //converting the 2 pin value to single number
  int sum  = (lastEncoded << 2) | encoded; //adding it to the previous encoded value

  if (sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) {
    encoderValue ++;
    encoderDirection = "1";
  }
  if (sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) {
    encoderValue --;
    encoderDirection = "2";
  }

  lastEncoded = encoded; //store this value for next time
  Serial.println(encoderDirection);
}

void rotateCCW() {
  switch (active_step_byte) {
    case 1:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      active_step_byte = 2;
      break;
    case 2:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      active_step_byte = 3;
      break;
    case 3:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      active_step_byte = 4;
      break;
    case 4:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      active_step_byte = 1;
      break;
  }
  delayMicroseconds(delay_step_speed);
}

void rotateCW() {
  switch (active_step_byte) {
    case 1:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, HIGH);
      active_step_byte = 4;
      break;
    case 2:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      active_step_byte = 1;
      break;
    case 3:
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, LOW);
      active_step_byte = 2;
      break;
    case 4:
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      active_step_byte = 3;
      break;
  }
  delayMicroseconds(delay_step_speed);
}
