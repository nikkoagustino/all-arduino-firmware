// Simple example application that shows how to read four Arduino
// digital pins and map them to the USB Joystick library.
//
// Ground digital pins 9, 10, 11, and 12 to press the joystick 
// buttons 0, 1, 2, and 3.
//
// NOTE: This sketch file is for use with Arduino Leonardo and
//       Arduino Micro only.
//
// by Matthew Heironimus
// 2015-11-20
//--------------------------------------------------------------------

#include <Joystick.h>

Joystick_ Joystick;

const int beaconLightSwitch = 15;
const int landingLightSwitch = 18;
const int taxiLightSwitch = 19;
const int navLightSwitch = 20;
const int strobeLightSwitch = 21;

const int fuelPumpSwitch = 16;
const int pitotHeatSwitch = 14;

const int starterSwitch = A9;

const int standbyBattArmSwitch = 6;
const int standbyBattTest = 7;

String hardwareInput = "";

void setup() {
  // Initialize Button Pins
  // 7 toggle switch
  pinMode(beaconLightSwitch, INPUT_PULLUP);
  pinMode(landingLightSwitch, INPUT_PULLUP);
  pinMode(taxiLightSwitch, INPUT_PULLUP);
  pinMode(navLightSwitch, INPUT_PULLUP);
  pinMode(strobeLightSwitch, INPUT_PULLUP);
  pinMode(fuelPumpSwitch, INPUT_PULLUP);
  pinMode(pitotHeatSwitch, INPUT_PULLUP);
  // 1 standby battery
  pinMode(standbyBattArmSwitch, INPUT_PULLUP);
  pinMode(standbyBattTest, INPUT_PULLUP);

  // Initialize Joystick Library
  Joystick.begin();
}

// Constant that maps the phyical pin to the joystick button.
const int pinToButtonMap = 40;

// Last state of the button
int lastButtonState[12] = {0,0,0,0,0,0,0,0,0,0,0,0};

void loop() {

  // Read pin values
  Joystick.setButton(0, !digitalRead(beaconLightSwitch));
  Joystick.setButton(1, !digitalRead(landingLightSwitch));
  Joystick.setButton(2, !digitalRead(taxiLightSwitch));
  Joystick.setButton(3, !digitalRead(navLightSwitch));
  Joystick.setButton(4, !digitalRead(strobeLightSwitch));
  Joystick.setButton(5, !digitalRead(fuelPumpSwitch));
  Joystick.setButton(6, !digitalRead(pitotHeatSwitch));
  Joystick.setButton(7, !digitalRead(standbyBattArmSwitch));
  Joystick.setButton(8, !digitalRead(standbyBattTest));

  // reset starter to 0
  for (int i = 0; i <= 4; i++) {
    Josystick.setButton(9 + i, 0);
  }
  int starterPosition = analogRead(starterSwitch);
  if (starterPosition > 1000) {
    Joystick.setButton(9, 1); //mag R
  } else if (starterPosition > 600) && (starterPosition < 1000) {
    Joystick.setButton(10, 1); //mag L
  } else if (starterPosition > 100) && (starterPosition < 400) {
    Joystick.setButton(11, 1); //mag both
  } else if (starterPosition < 100) {
    Joystick.setButton(12, 1); //starter
  }

  // other switch set 0
  for (int i = 13; i <= 32; i++) {
    Josystick.setButton(i, 0);
  }
  
  delay(50);
}

