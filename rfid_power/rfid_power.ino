#include <SoftwareSerial.h>
#include <Wire.h>
const int DS1307 = 0x68; // Address of DS1307 see data sheets
const char* days[] =
{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
const char* months[] =
{"January", "February", "March", "April", "May", "June", "July", "August","September", "October", "November", "December"};

char incomingByte[10];
int terminateByte = 10;
 
// Initializes all values: 
byte second = 0;
byte minute = 0;
byte hour = 0;
byte weekday = 0;
byte monthday = 0;
byte month = 0;
byte year = 0;


SoftwareSerial RFID(2, 3);
const int SSRPin = 4;
const int RFIDEnablePin = 7;
const int RFIDDisablePin = 6;
int cardID;
int cardLength;
int tmp;
boolean activateSSR = false;
boolean checkCard = true;
byte lastRFIDDetectedMinute = 0;
byte lastRFIDDetectedSecond = 0;
byte standbyTimeBeforeShutdown = 10;

int data1 = 0;
int ok = -1;
int yes = 13;
int no = 12;
int tag1[14] = {2,48,48,48,48,53,69,68,51,50,49,65,67,3}; // master
int tag2[14] = {2,48,48,48,48,53,67,70,66,68,51,55,52,3}; //64447
//int tag2[14] = {2,48,48,48,48,53,69,68,51,51,65,66,55,3}; //54074
//int tag2[14] = {2,48,48,48,48,53,69,54,56,53,67,54,65,3}; // 26716
int newtag[14] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void setup() {
  // put your setup code here, to run once:
  Wire.begin();
  pinMode(SSRPin, OUTPUT);
  pinMode(RFIDEnablePin, OUTPUT);
  Serial.begin(9600);
  RFID.begin(9600);
// setTimeViaSerialMonitor();
}

void setTimeViaSerialMonitor() {
  delay(2000); // This delay allows the MCU to read the current date and time.
  
  Serial.print("The current date and time is: ");
  printTime();
  Serial.println("Please change to newline ending the settings on the lower right of the Serial Monitor");
  Serial.println("Would you like to set the date and time now? Y/N");

  while (!Serial.available()) delay(10);
  if (Serial.read() == 'y' || Serial.read() == 'Y')
  // This set of functions allows the user to change the date and time
  {
    Serial.read();
    setTime();
    Serial.print("The current date and time is now: ");
    printTime();
  }
}

byte decToBcd(byte val) { 
  return ((val/10*16) + (val%10));
}
byte bcdToDec(byte val) {
  return ((val/16*10) + (val%16));
}
// This set of codes is allows input of data
void setTime() {
  Serial.print("Please enter the current year, 00-99. - ");
  year = readByte();
  Serial.println(year);
  Serial.print("Please enter the current month, 1-12. - ");
  month = readByte();
  Serial.println(months[month-1]);
  Serial.print("Please enter the current day of the month, 1-31. - ");
  monthday = readByte();
  Serial.println(monthday);
  Serial.println("Please enter the current day of the week, 1-7.");
  Serial.print("1 Sun | 2 Mon | 3 Tues | 4 Weds | 5 Thu | 6 Fri | 7 Sat - ");
  weekday = readByte();
  Serial.println(days[weekday-1]);
  Serial.print("Please enter the current hour in 24hr format, 0-23. - ");
  hour = readByte();
  Serial.println(hour);
  Serial.print("Please enter the current minute, 0-59. - ");
  minute = readByte();
  Serial.println(minute);
  second = 0;
  Serial.println("The data has been entered.");
 
  // The following codes transmits the data to the RTC
  Wire.beginTransmission(DS1307);
  Wire.write(byte(0));
  Wire.write(decToBcd(second));
  Wire.write(decToBcd(minute));
  Wire.write(decToBcd(hour));
  Wire.write(decToBcd(weekday));
  Wire.write(decToBcd(monthday));
  Wire.write(decToBcd(month));
  Wire.write(decToBcd(year));
  Wire.write(byte(0));
  Wire.endTransmission();
  // Ends transmission of data
}

byte readByte() {
  while (!Serial.available()) delay(10);
  byte reading = 0;
  byte incomingByte = Serial.read();
  while (incomingByte != '\n') {
    if (incomingByte >= '0' && incomingByte <= '9')
      reading = reading * 10 + (incomingByte - '0');
    else;
    incomingByte = Serial.read();
  }
  Serial.flush();
  return reading;
}

void printTime() {
  char buffer[3];
  const char* AMPM = 0;
  readTime();
  Serial.print(days[weekday-1]);
  Serial.print(" ");
  Serial.print(months[month-1]);
  Serial.print(" ");
  Serial.print(monthday);
  Serial.print(", 20");
  Serial.print(year);
  Serial.print(" ");
  Serial.print(hour);
  Serial.print(":");
//  Serial.print(minute);
  sprintf(buffer, "%02d", minute);
  Serial.print(buffer);
  Serial.print(":");
  sprintf(buffer, "%02d", second);
  Serial.println(buffer);
}

void readTime() {
  Wire.beginTransmission(DS1307);
  Wire.write(byte(0));
  Wire.endTransmission();
  Wire.requestFrom(DS1307, 7);
  second = bcdToDec(Wire.read());
  minute = bcdToDec(Wire.read());
  hour = bcdToDec(Wire.read());
  weekday = bcdToDec(Wire.read());
  monthday = bcdToDec(Wire.read());
  month = bcdToDec(Wire.read());
  year = bcdToDec(Wire.read());
}

boolean comparetag(int aa[14], int bb[14])
{
  boolean ff = false;
  int fg = 0;
  for (int cc = 0 ; cc < 14 ; cc++)
  {
    if (aa[cc] == bb[cc])
    {
      fg++;
    }
  }
  if (fg == 14)
  {
    ff = true;
  }
  return ff;
}

void loop() {
  // put your main code here, to run repeatedly:
  readTime();
  char cardIDFull[14];
  if (RFID.available() > 0)
  {
     cardID = RFID.read();
     Serial.print(cardID, DEC);
     newtag[cardLength] = cardID;
     cardLength++;
     if (cardLength == 14) {
      Serial.println("");
      cardLength = 0;
      RFID.flush();
      if ((comparetag(newtag, tag1) == true) || (comparetag(newtag, tag2) == true))
      {
        ok++;
        Serial.println("Accepted");
        lastRFIDDetectedMinute = second;
        lastRFIDDetectedSecond = second;
        activateSSR = true;
        digitalWrite(RFIDEnablePin, HIGH);
        digitalWrite(RFIDDisablePin, LOW);
       } else {
        Serial.println("Rejected");
       }
       printTime();
     }
  }

  if (Serial.available() > 0) {
    Serial.readBytesUntil(terminateByte, incomingByte, 10);
    Serial.println(incomingByte);
    memset(incomingByte, 0, sizeof(incomingByte));
  }

  int secondMin = second - standbyTimeBeforeShutdown;
  if ((secondMin <= 0) && (lastRFIDDetectedMinute > (60 - standbyTimeBeforeShutdown))) { secondMin = secondMin + 60; }
  if (lastRFIDDetectedMinute < secondMin) {
    activateSSR = false;
  }

  if ((lastRFIDDetectedSecond  == 59) && (second == 0)) {
    digitalWrite(RFIDEnablePin, LOW);
    digitalWrite(RFIDDisablePin, HIGH);
  }
  if (lastRFIDDetectedSecond < second - 1) {
    digitalWrite(RFIDEnablePin, LOW);
    digitalWrite(RFIDDisablePin, HIGH);
  }

  if (activateSSR == true) {
    digitalWrite(SSRPin, HIGH);
  } else {
    digitalWrite(SSRPin, LOW);
  }
}
