//these pins can not be changed 2/3 are special pins
const int encoderPin1 = 2;
const int encoderPin2 = 4;
int encoderValue = 0;
int pin1Val, pin2Val;

void setup() {
  Serial.begin (9600);
  pinMode(encoderPin1, INPUT_PULLUP);
  pinMode(encoderPin2, INPUT_PULLUP);
  attachInterrupt(0, rotaryInterrupt, CHANGE);
//  attachInterrupt(1, rotaryInterrupt, CHANGE);
}

void loop(){ 
  //Do stuff here
//rotaryInterrupt();
}

void rotaryInterrupt()
{
  pin1Val = digitalRead(encoderPin1);
  pin2Val = digitalRead(encoderPin2);
  if (pin1Val == LOW) {
    if (pin2Val == HIGH) {
      Serial.println("CW");
    } else {
      Serial.println("CCW");
    }
  }
}

