#include <SoftwareSerial.h>

const int SN74HC165_RESET = 2;
const int SN74HC165_CLOCK = 3;
const int SN74HC165_DATA = 4;
const int numberOf74HC165 = 2;
String inputDataString;
//const int 74HC595_CLOCK = 5;
//const int 74HC595_DATA = 6;
//const int 74HC595_LATCH = 7;
////const int numberOf74HC595 = 1;
//char incomingByte[10];
//int terminateByte = 10;

void setup() {
  // put your setup code here, to run once:
  pinMode(SN74HC165_RESET, OUTPUT);
  pinMode(SN74HC165_CLOCK, OUTPUT);
  pinMode(SN74HC165_DATA, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  read_74HC165();
//  write_74HC595();
}

void reset_74HC165() {
  digitalWrite(SN74HC165_RESET, LOW);
  digitalWrite(SN74HC165_RESET, HIGH);
}

void clock_74HC165() {
  digitalWrite(SN74HC165_CLOCK, HIGH);
  digitalWrite(SN74HC165_CLOCK, LOW);
}

void read_74HC165() {
  inputDataString = "";
  reset_74HC165();
  for (int i = 1; i <= 8 * numberOf74HC165; i++) {
    if (digitalRead(SN74HC165_DATA) == HIGH) {
      inputDataString = String(inputDataString + "1");
    } else {
      inputDataString = String(inputDataString + "0");
    }
    clock_74HC165();
  }
  Serial.println(inputDataString);
}
//
//void clock_74HC595() {
//  digitalWrite(74HC595_CLOCK, HIGH);
//  digitalWrite(74HC595_CLOCK, LOW);
//}
//
//void latch_74HC595() {
//  digitalWrite(74HC595_LATCH, HIGH);
//  digitalWrite(74HC595_LATCH, LOW);
//}

//void readIncomingData() {
//  if (Serial.available() > 0) {
//    Serial.readBytesUntil(terminateByte, incomingByte, 10);
//    Serial.println(incomingByte);
//    memset(incomingByte, 0, sizeof(incomingByte));
//  }
//}
//
//void write_74HC595() {
//  for (int i = 0; i <= terminateByte; i++) {
//    if (incomingByte[i] == 1) {
//      digitalWrite(74HC595_DATA, HIGH);
//    } else {
//      digitalWrite(74HC595_DATA, LOW);
//    }
//    clock_74HC595();
//  }
//  latch_74HC595();
//}

