const int EmergencyPower = 3;
const int PLNDetection = 2;

void setup() {
  // put your setup code here, to run once:
  pinMode(EmergencyPower, OUTPUT);
  pinMode(PLNDetection, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (digitalRead(PLNDetection) == LOW) {
    digitalWrite(EmergencyPower, HIGH);
  } else {
    digitalWrite(EmergencyPower, LOW);
  }
}
